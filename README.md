# Barcode Swiss Army Knife
This android application can be used to scan, alter and generate barcodes.

## Scan
Scan a generic barcode. Detects type and code.

## Alter
Change the barcode data. Checksum generation is optional.

## Generate
Generate common barcode types and display on screen.
